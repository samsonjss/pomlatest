package com.framework.testcases;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;



//testcase for login and logout using POM

public class TC_001 extends ProjectMethods {
	
	
	@BeforeTest
	public void setdata() {
		
		/*LoginPage lp = new LoginPage();
		lp.enterUsername("");
		lp.enterPassword("");
		lp.clickLogin();*/
		
		testCaseName ="TC001_LoginLogout";
		
		testDescription="Login to leaftaps";
		
		testNodes="Leads";
		
		author="SAMSON";
		
		category="smoke";
		
		dataSheetName="TC_001";
	}
	
	@Test(dataProvider="fetchData")
	public void login(String username, String password) {
		
		new LoginPage()
		.enterUsername(username)
		.enterPassword(password)
		.clickLogin();
		
		
	}
	

}
